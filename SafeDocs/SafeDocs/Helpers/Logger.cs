﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SafeDocs.Helpers
{
    public enum MessageState
    {
        INFO = 0,
        INFO2 = 1,
        IMPORTANT_INFO = 2,
        WARNING = 3,
        ERROR = 4,
        ERROR_FATAL = 5,
        SUCCES = 6,
    }
    public class Logger
    {
        private const ConsoleColor COLOR_1 = ConsoleColor.Cyan;
        private const ConsoleColor COLOR_2 = ConsoleColor.DarkCyan;
        private const ConsoleColor INFORMATION = ConsoleColor.Green;
        private const ConsoleColor SHOP = ConsoleColor.Yellow;
        private const ConsoleColor PLAYER = ConsoleColor.Blue;
        private const ConsoleColor UNSET = ConsoleColor.Gray;

        private static Dictionary<MessageState, ConsoleColor> Colors = new Dictionary<MessageState, ConsoleColor>()
        {
            { MessageState.INFO,            ConsoleColor.Gray },
            { MessageState.INFO2,           ConsoleColor.DarkGray },
            { MessageState.IMPORTANT_INFO,  ConsoleColor.White },
            { MessageState.SUCCES,          ConsoleColor.Green },
            { MessageState.WARNING,         ConsoleColor.Yellow },
            { MessageState.ERROR ,          ConsoleColor.DarkRed},
            { MessageState.ERROR_FATAL,     ConsoleColor.Red }
        };

        public static void WriteSuccess(object value)
        {
            Write(value, MessageState.SUCCES);
        }
        public static void WriteError(object value)
        {
            Write(value, MessageState.ERROR);
        }

        public static void Write(object value, MessageState state = MessageState.INFO)
        {
            WriteColored(value, Colors[state]);
        }

        private static void WriteColored(object value, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(value);
        }
    }
}
