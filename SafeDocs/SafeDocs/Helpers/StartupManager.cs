﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace SafeDocs.Helpers
{
    public class StartupManager
    {
        public static List<Func<string>> StartupMethods = new List<Func<string>>();

        public static void LoadMethods()
        {
            StartupMethods.Add( Testing.FileTesting.CheckDictionary );
            StartupMethods.Add( Testing.FileTesting.CheckCities );
            StartupMethods.Add( Testing.FileTesting.CheckTesseract );
            StartupMethods.Add( Testing.FileTesting.CheckUpdater);

        }

        public static void Initialize()
        {
            LoadMethods();
            Logger.Write("** Uruchamianie **");
            Stopwatch watch = Stopwatch.StartNew();
            TimeSpan ts = watch.Elapsed;

            foreach (Func<string> a in StartupMethods)
            {
                Logger.Write(a.Invoke(), Testing.FileTesting.state);
            }

            watch.Stop();
            Logger.Write("** Zaladowano w: " + String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10) + " **");
        }
    }
}
