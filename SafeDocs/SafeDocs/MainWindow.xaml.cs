﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SafeDocs.Helpers;
using iText.Kernel.Pdf;
using SharpGL;

namespace SafeDocs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region debug
        [Conditional("DEBUG")]
        [DllImport("Kernel32")]
        public static extern void AllocConsole();
        [DllImport("Kernel32")]
        public static extern void FreeConsole();
        #endregion

        #region konstruktory
        Windows.OCR ocrWindow;
        Windows.MultiOCR multiOCRWindow;
        #endregion


        public MainWindow()
        {
            AllocConsole();
            int final = 10;
            Task t1 = new Task(() =>
            { StartupManager.Initialize(); }
            );
            t1.Start();


            this.ocrWindow = new Windows.OCR();
            this.multiOCRWindow = new Windows.MultiOCR();

            InitializeComponent();
            this.allPDFs.AddHandler(TreeViewItem.SelectedEvent, new RoutedEventHandler(treeItem_Selected));

        }

        private void closeWindow(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void minWindow(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void openFile(object sender, RoutedEventArgs e)
        {
            if (this.pdfView.OpenFile(null))
            {

                TreeViewItem item = new TreeViewItem();

                if ((int)System.IO.Path.GetFileNameWithoutExtension(this.pdfView.filePath).Length >= 20)
                {
                    item.Header = System.IO.Path.GetFileNameWithoutExtension(this.pdfView.filePath).Substring(0, 20) + ".." + ".pdf";

                }
                else
                {
                    item.Header = System.IO.Path.GetFileNameWithoutExtension(this.pdfView.filePath) + ".pdf";

                }
                item.Tag = this.pdfView.filePath;
                this.allPDFs.Items.Add(item);

                if (this.allPDFs.Items.Count > 1)
                {
                    this.Home.multiPDF.IsEnabled = true;
                }
            }

        }


        private void runOCR(object sender, RoutedEventArgs e)
        {
            this.ocrWindow.Show();
        }


        private void openIMGToPDFBox(object sender, RoutedEventArgs e)
        {
            this.multiOCRWindow.Show();
        }
        
        private void treeItem_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            TreeViewItem selectedItem = allPDFs.SelectedItem as TreeViewItem;
            this.pdfView.OpenFile(selectedItem.Tag.ToString());
        }

    }
}
