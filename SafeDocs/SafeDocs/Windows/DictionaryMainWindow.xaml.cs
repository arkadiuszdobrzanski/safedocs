﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SafeDocs.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy DictionaryMainWindow.xaml
    /// </summary>
    public partial class DictionaryMainWindow : Window
    {
       
        private Modules.DictionaryModule dictionaryModule;
        private List<string> dictionaryEntries;
        private string filePath;

        public DictionaryMainWindow()
        {
            InitializeComponent();
            this.dictionaryModule = new Modules.DictionaryModule();
        }

        public void Load(string filePath)
        {
            this.filePath = filePath;
            this.dictionaryEntries = this.dictionaryModule.GetDictionary(filePath);
            foreach(var item in this.dictionaryEntries)
            {
                this.dictionary.Items.Add(item);
            }
        }

        private void addToDictionary_Click(object sender, RoutedEventArgs e)
        {
            this.dictionaryModule.AddToDictionary(this.filePath, d_inputValue.Text);
            this.dictionary.Items.Add(d_inputValue.Text);
        }

        private void deleteFromDictionary_Click(object sender, RoutedEventArgs e)
        {
            if (this.dictionary.SelectedItem == null)
            {
                this.deleteFromDictionary.IsEnabled = false;
            }
            else
            {
                this.dictionaryModule.RemoveFromDicionary(this.filePath, this.dictionary.SelectedItem.ToString());
                this.dictionary.Items.RemoveAt(this.dictionary.Items.IndexOf(this.dictionary.SelectedItem));
                this.deleteFromDictionary.IsEnabled = false;
            }
        }

        private void saveDictionary_Click(object sender, RoutedEventArgs e)
        {
            this.dictionaryModule.SaveDictionary(this.filePath);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void dictionary_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.deleteFromDictionary.IsEnabled = true;
        }
    }
    
}
