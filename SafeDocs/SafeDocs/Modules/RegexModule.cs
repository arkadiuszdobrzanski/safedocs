﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SafeDocs.Modules
{
    class RegexModule
    {

        public const string PESEL = @"[0-9]{11}";
        public const string POST_CODE = @"[0-9]{2}-[0-9]{3}";
        public const string PHONE = @"/^\+48\d{9}$/";
        public const string CAR_PLATE = @"[A-Z,0-9]{1,3} [A-Z,0-9]{1,6}";
        public const string EMAIL = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

    }
}
