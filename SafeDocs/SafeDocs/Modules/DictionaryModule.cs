﻿using SafeDocs.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SafeDocs.Modules
{
    public class DictionaryModule
    {
        private List<string> _arrayDictionary = new List<string> { "" };
        private List<string> _arrayDictionaries = new List<string> { "" };
        private string line;
        public DirectoryInfo dinfo = new DirectoryInfo(@"./");

        public DictionaryModule()
        {

        }

        public List<string> GetDictionary(string name)
        {
            _arrayDictionary.Clear();
            if (File.Exists(@"./" + name + ".txt"))
            {
                var file = new StreamReader(@"./" + name + ".txt");
                int i = 0;
                while ((line = file.ReadLine()) != null)
                {
                    this._arrayDictionary.Add(line);

                }
                Logger.WriteSuccess("Pomyślnie pobrano słownik o nazwie " + name + ".txt");
            }
            else
            {
                Logger.WriteError("Taki plik nie istnieje");
            }
            return this._arrayDictionary;
        }

        public List<string> GetDictionaries()
        {
            this._arrayDictionaries.Clear();
            foreach (FileInfo file in this.dinfo.GetFiles("*.txt"))
            {
                this._arrayDictionaries.Add(Path.GetFileNameWithoutExtension(file.Name));
            }
            Logger.WriteSuccess("Załadowano listę słowników");
            return this._arrayDictionaries;
        }


        public void CreateDictionary(string name)
        {
            if (!File.Exists(@"./" + name + ".txt"))
            {
                File.Create(@"./" + name + ".txt");
            }
            else
            {
                Logger.WriteError("Taki słownik już istnieje!");
            }
        }

        public void AddToDictionary(string name, string line)
        {
            if (File.Exists(@"./" + name + ".txt"))
            {
                this._arrayDictionary.Add(line);
            }
            else
            {
                Logger.WriteError("Taki słownik nie istnieje!");
            }
        }

        public void RemoveFromDicionary(string name, string line)
        {
            if (File.Exists(@"./" + name + ".txt"))
            {
                this._arrayDictionary.RemoveAt(this._arrayDictionary.IndexOf(line));
            }
            else
            {
                Logger.WriteError("Taki słownik nie istnieje!");
            }
        }
        //

        public bool SaveDictionary(string name)
        {
            StreamWriter SaveFile = new StreamWriter(@"./" + name + ".txt");
            foreach (var item in this._arrayDictionary)
            {
                SaveFile.WriteLine(item.ToString());
            }
            SaveFile.Close();
            return true;
        }

        public void RemoveDictionary(string name)
        {
            if (File.Exists(@"./" + name + ".txt"))
            {
                File.Delete(@"./" + name + ".txt");
            }
            else
            {
                Logger.WriteError("Taki plik nie istnieje");
            }
        }

    }
}
