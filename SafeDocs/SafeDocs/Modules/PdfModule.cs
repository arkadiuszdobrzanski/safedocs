﻿using Microsoft.Win32;
using SafeDocs.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace SafeDocs.Modules
{
    public class PdfModule
    {
        public string _fileName;
        public string _filePath;

        public List<string> _fileNames;
        public List<string> _filePaths;
        public List<string> _metadata;
        
        
        public List<string> OpenPDF(bool multi = false)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki PDF (*.pdf)|*.pdf|Wszystkie pliki (*.*)|*.*";
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (String file in openFileDialog.FileNames)
                {
                    this._filePaths.Add(file);

                }
            }
            if (this._filePaths != null || this._filePaths.Count != 0)
            {
                MessageBox.Show("Wykryto " + this._filePaths.Count + " plików PDF.");
                return _filePaths;
            }
            else
            {
                Logger.WriteError("błąd przy wybieraniu plików");
                return null;
            }
        }

        public void GetPdfInfo()
        {

        }

        public List<string> GetAllPDFsFilePaths()
        {
            return this._filePaths;
        }
        public List<string> GetAllPDFsFileNames()
        {
            return this._fileNames;
        }

        public void SetMetaData(List<string> metadata)
        {
            
        }

        public List<string> GetMetaData()
        {
            return this._metadata;
        }
        public void SaveAsPdf()
        {

        }

        public void CreatePDF()
        {

        }

        public void SetFileName(string _fileName)
        {
            this._fileName = _fileName;
        }

        public string GetFileName()
        {
            return _fileName;
        }

    }
}
