﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SafeDocs.Modules
{
    class Cities
    {
        private List<string> _arrayCities;
        public List<string> GetCities()
        {
            if (File.Exists(Config.CITIES_PATH))
            {
                var file = new StreamReader(Config.CITIES_PATH);
                int i = 0;
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    this._arrayCities[i] = line;
                    i++;
                }
            }
            return this._arrayCities;
        }


    }
}
