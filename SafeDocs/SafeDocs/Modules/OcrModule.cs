﻿using System.Collections.Generic;
using System.IO;
using System.Collections;
using Tesseract;
using System;
using System.Diagnostics;
using SafeDocs.Helpers;

namespace SafeDocs.Modules
{
    class OcrModule : PdfModule
    {
        
        public void convertToPdf(string filePath)
        {
            try
            {
                using (IResultRenderer renderer = Tesseract.PdfResultRenderer.CreatePdfRenderer(@"" + Path.GetDirectoryName(filePath) + "/" + Path.GetFileNameWithoutExtension(filePath), @".\tessdata\", false))
                {
                    using (renderer.BeginDocument(Path.GetFileNameWithoutExtension(filePath)))
                    {
                        string configurationFilePath = @".\tessdata";
                        string configfile = Path.Combine(@".\tessdata", "pdf");
                        using (TesseractEngine engine = new TesseractEngine(configurationFilePath, "pol", EngineMode.TesseractAndLstm, configfile))
                        {
                            using (Pix img = Pix.LoadFromFile(filePath))
                            {
                                using (var page = engine.Process(img, Path.GetFileNameWithoutExtension(filePath)))
                                {
                                    renderer.AddPage(page);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteError(e.Message);
            }
            
        }
    }
}
