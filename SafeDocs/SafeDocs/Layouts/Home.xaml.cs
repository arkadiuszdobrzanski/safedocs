﻿using iText.Kernel.Pdf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SafeDocs.Layouts
{
    /// <summary>
    /// Logika interakcji dla klasy Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {

        #region konstruktory
        Windows.MultiOCR multOCRWindow;
        Windows.OCR ocrWindow;
        Windows.Regex regexWindow;
        Layouts.Dictionaries dictionariesLayout;
        #endregion


        public Home()
        {
            this.dictionariesLayout = new Layouts.Dictionaries();
            InitializeComponent();
        }

        private void execute_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
         
        }
        private void Reset_click(object sender, RoutedEventArgs e)
        {
       
        }
        private void settingsCLick(object sender, RoutedEventArgs e)
        {
           
        }
        private void MultiPDF(object sender, RoutedEventArgs e)
        {
        
        }

        private void editDictionary(object sender, RoutedEventArgs e)
        {
            this.dictionariesLayout.Show();
        }

        private void chooseFile_Click(object sender, RoutedEventArgs e)
        {
            
        }
        
       
        private void chooseClassic_Click(object sender, RoutedEventArgs e)
        {
            

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void openSettings(object sender, RoutedEventArgs e)
        {

        }
    }
    
}
