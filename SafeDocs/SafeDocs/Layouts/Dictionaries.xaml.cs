﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SafeDocs.Helpers;

namespace SafeDocs.Layouts
{
    /// <summary>
    /// Logika interakcji dla klasy Dictionaries.xaml
    /// </summary>
    public partial class Dictionaries : Window
    {
        public Modules.DictionaryModule dictionaryModule;
        public Windows.DictionaryMainWindow dictionaryMainWindow;
        public Dictionaries()
        {

            InitializeComponent();
            this.dictionaryModule = new Modules.DictionaryModule();
            this.Refresh();
            this.dictionariesList.AddHandler(TreeViewItem.SelectedEvent, new RoutedEventHandler(ActivateButtons));
            //this.AddHandler(MouseUpEvent, new RoutedEventHandler(Refresh));
            //this.AddHandler(MouseDownEvent, new RoutedEventHandler(Refresh));
        }

        public void ActivateButtons(object sender, RoutedEventArgs e)
        {
            this.editButton.IsEnabled = true;
            this.removeButton.IsEnabled = true;

        }  
      

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.dictionaryModule.CreateDictionary(this.dictionaryName.Text);
            this.Refresh();
        }

        public void Refresh()
        {
            this.dictionariesList.Items.Clear();
            foreach(var foo in this.dictionaryModule.GetDictionaries())
            {
                TreeViewItem item = new TreeViewItem();
                item.Header = foo;
                item.Tag = foo;
                this.dictionariesList.Items.Add(item);
                this.dictionariesList.FontSize = 20;
            }

            if (this.dictionariesList.SelectedItem == null)
            {
                this.editButton.IsEnabled = false;
                this.removeButton.IsEnabled = false;
            }
        }

        private void buttonEdit(object sender, RoutedEventArgs e)
        {
            TreeViewItem selectedItem = this.dictionariesList.SelectedItem as TreeViewItem;
            
            this.dictionaryMainWindow = new Windows.DictionaryMainWindow();
            this.dictionaryMainWindow.Show();
            this.dictionaryMainWindow.Load(selectedItem.Header.ToString());
            this.Refresh();
        }

        private void buttonDelete(object sender, RoutedEventArgs e)
        {
            TreeViewItem selectedItem = this.dictionariesList.SelectedItem as TreeViewItem;
          
            this.dictionaryModule.RemoveDictionary(selectedItem.Header.ToString());
            this.Refresh();
        }
    }
}
