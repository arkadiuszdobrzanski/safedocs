﻿using SafeDocs.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SafeDocs.Testing
{

    public class FileTesting
    {
        static public MessageState state = MessageState.ERROR;

        public static string CheckDictionary()
        {

            if (!File.Exists(@"./dictionary.txt"))
            {
                state = MessageState.ERROR;
                return "Nie znaleziono pliku dictionary.txt";
            }
            state = MessageState.SUCCES;
            return "Plik słownika załadowany";

        }

        public static string CheckCities()
        {
            if (!File.Exists(@"./cities.txt"))
            {
                state = MessageState.ERROR;
                return "Nie znaleziono pliku cities.txt";
            }
            state = MessageState.SUCCES;
            return "Plik z miastami załadowany";
        }

        public static string CheckTesseract()
        {
            if (!Directory.Exists(@"./tessdata"))
            {
                state = MessageState.ERROR;
                return "Brak folderu tessdata";
            }
            else
            {
                if (!File.Exists(@"./pol.traineddata"))
                {
                    state = MessageState.ERROR;
                    return "Brak pliku pol.traineddata";
                }
            }
            state = MessageState.SUCCES;
            return "Znaleziono plik Tesseracta";
        }

        public static string CheckUpdater()
        {
            if (!Directory.Exists(@"./SafeDocsUpdater"))
            {
                state = MessageState.ERROR;
                return "Brak autoupdatera";
            }
            state = MessageState.SUCCES;
            return "Plik Autoupdater załadowany";
        }


        

    }
}
